SOCIALTWIST TELL-A-FRIEND MODULE - INSTALL
------------------------------------------
Tell-a-Friend widget to let your visitors reach their friends on Email, IM or Social Networks without leaving your site.

Installation
------------

1. Copy the "socialtwist_taf" directory to the <drupal installation dir>/sites/all/modules or sites/example.com/modules/ directory.
    * If modules folder doesn't exists you have to create one and copy the "socialtwist_taf" folder to that.
2. Go to Administer -> Site Building -> Modules 
    * Select the Module "SocialTwist Tell-a-Friend" and click on Save Configuration.
3. Go to Administer -> Site configuration -> SocialTwist Tell-a-friend. 
    * Fill in your SocialTwist Account ID obtained by registering at http://tellafriend.socialtwist.com
    * Select the side to align widget on page
    * Select the content types where to embed widget
4.  Go to the Administer -> Site Building -> Blocks 
       Under Disabled you will find the "SocialTwist Tell-a-Friend". Select one of the Region options in the dropdown  and click on SaveBlocks.